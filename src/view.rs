use ggez::graphics;
use ggez::graphics::{Point2, Rect, TextCached};
use ggez::{Context, GameResult};
use grid::{Coordinate, Grid, Region};

const BLOCK_SIZE: f32 = 10.0;
const MIN_ZOOM: f32 = 0.3;

pub struct View {
	pub cursor_pos: Coordinate,
	scr_width: f32,
	scr_height: f32,
	block_size: f32,
	zoom: f32,
	visible_rows: isize,
	visible_columns: isize,
	row_offset: isize,
	column_offset: isize,
}

impl View {
	pub fn new() -> View {
		let mut view = View {
			cursor_pos: (0, 0).into(),
			scr_width: 800.0,
			scr_height: 600.0,
			block_size: 10.0,
			zoom: 1.0,
			visible_rows: 0,
			visible_columns: 0,
			row_offset: 0,
			column_offset: 0,
		};

		view.recalculate();

		view
	}

	pub fn get_view_region(&self) -> Region {
		(
			self.cursor_pos.row - self.row_offset,
			self.cursor_pos.col - self.column_offset,
			self.cursor_pos.row + self.row_offset,
			self.cursor_pos.col + self.column_offset,
		)
			.into()
	}

	pub fn draw(&mut self, grid: &mut Grid, ctx: &mut Context) -> GameResult<()> {
		let region: Region = self.get_view_region();

		let live_cells = grid.live_cells_within(&region);
		let live_len = live_cells.len();

		for coordinate in live_cells {
			let rect = Rect::new(
				(coordinate.col - self.cursor_pos.col + self.column_offset) as f32 * self.block_size,
				(coordinate.row - self.cursor_pos.row + self.row_offset) as f32 * self.block_size,
				self.block_size,
				self.block_size,
			);

			graphics::set_color(ctx, graphics::WHITE)?;
			graphics::rectangle(ctx, graphics::DrawMode::Fill, rect)?;
		}

		graphics::set_color(ctx, graphics::Color::new(0.0, 1.0, 1.0, 1.0))?;
		graphics::points(
			ctx,
			&[Point2::new(
				self.column_offset as f32 * self.block_size + self.block_size / 4.0,
				self.row_offset as f32 * self.block_size + self.block_size / 4.0,
			)],
			self.block_size / 2.0,
		)?;

		graphics::set_color(ctx, graphics::WHITE)?;

		graphics::draw(
			ctx,
			&TextCached::new(format!(
				"cursor:({} {}) count:({})",
				self.cursor_pos.row, self.cursor_pos.col, live_len
			))?,
			Point2::new(10.0, 10.0),
			0.0,
		)?;

		Ok(())
	}

	pub fn pan(&mut self, rows: isize, cols: isize) {
		self.cursor_pos = (self.cursor_pos.row + rows, self.cursor_pos.col + cols).into();
		self.recalculate()
	}

	pub fn zoom_out(&mut self) {
		self.zoom = MIN_ZOOM.max(self.zoom - 0.1);
		self.block_size = self.zoom * BLOCK_SIZE;
		self.recalculate();
	}

	pub fn zoom_in(&mut self) {
		self.zoom += 0.1;
		self.block_size = self.zoom * BLOCK_SIZE;
		self.recalculate();
	}

	pub fn resize(&mut self, width: u32, height: u32) {
		self.scr_height = height as f32;
		self.scr_width = width as f32;
		self.recalculate();
	}

	fn recalculate(&mut self) {
		self.visible_rows = (self.scr_height / self.block_size).ceil() as isize;
		self.visible_columns = (self.scr_width / self.block_size).ceil() as isize;
		self.row_offset = self.visible_rows / 2;
		self.column_offset = self.visible_columns / 2;
	}
}
