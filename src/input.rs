use ggez::event::Keycode;
use std::collections::HashMap;

pub struct InputState {
	actions: HashMap<InputAction, InputActionState>,
	bindings: HashMap<Keycode, InputAction>,
}

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
pub enum InputAction {
	Up,
	Down,
	Left,
	Right,
	NudgeUp,
	NudgeDown,
	NudgeLeft,
	NudgeRight,
	ZoomIn,
	ZoomOut,
	FlipCell,
	Pause,
	Quit,
}

struct InputActionState {
	pub active: bool,
	pub repeats: bool,
}

impl InputState {
	pub fn new() -> InputState {
		InputState {
			actions: HashMap::new(),
			bindings: HashMap::new(),
		}
	}

	pub fn on_key_change(&mut self, key: Keycode, pressed: bool) {
		match self.resolve_binding(key) {
			Some(action) => {
				self
					.actions
					.entry(action)
					.and_modify(|a| a.active = pressed);
			}
			_ => (),
		}
	}

	pub fn set_binding(&mut self, action: InputAction, key: Keycode, repeats: bool) -> &mut Self {
		self
			.actions
			.entry(action)
			.and_modify(|a| a.repeats = repeats)
			.or_insert(InputActionState {
				active: false,
				repeats,
			});
		self.bindings.insert(key, action);
		self
	}

	pub fn is_active(&mut self, action: InputAction) -> bool {
		match self.actions.get_mut(&action) {
			Some(state) => {
				if !state.repeats && state.active {
					state.active = false;
					return true;
				}
				state.active
			}
			_ => false,
		}
	}

	fn resolve_binding(&self, key: Keycode) -> Option<InputAction> {
		self.bindings.get(&key).cloned()
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_unknown_binding() {
		let mut input_state = InputState::new();
		assert!(input_state.is_active(InputAction::Down) == false);
	}

	#[test]
	fn test_inactive_binding() {
		let mut input_state = InputState::new();
		input_state.set_binding(InputAction::Up, Keycode::W, true);
		assert!(input_state.is_active(InputAction::Up) == false)
	}

	#[test]
	fn test_active_binding() {
		let mut input_state = InputState::new();
		input_state.set_binding(InputAction::Up, Keycode::W, true);
		input_state.on_key_change(Keycode::W, true);
		assert!(input_state.is_active(InputAction::Up));
	}

	#[test]
	fn test_toggled_binding() {
		let mut input_state = InputState::new();
		input_state.set_binding(InputAction::Up, Keycode::W, true);
		input_state.on_key_change(Keycode::W, true);
		input_state.on_key_change(Keycode::W, false);
		assert!(input_state.is_active(InputAction::Up) == false);
	}

	#[test]
	fn test_non_repeating_binding() {
		let mut input_state = InputState::new();
		input_state.set_binding(InputAction::Up, Keycode::W, false);
		input_state.on_key_change(Keycode::W, true);
		assert!(input_state.is_active(InputAction::Up));
		assert!(input_state.is_active(InputAction::Up) == false);
	}

}
