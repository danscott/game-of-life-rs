use grid::Coordinate;

pub fn load_life(life: String) -> Vec<Coordinate> {
	let mut coordinates = Vec::new();
	let mut row = 0;
	for line in life.split('\n') {
		if line.starts_with('#') == false {
			let mut line_coords: Vec<Coordinate> = line
				.char_indices()
				.filter_map(|(i, c)| match c {
					'*' => Some((row as isize, i as isize).into()),
					_ => None,
				}).collect();

			coordinates.append(&mut line_coords);

			row += 1
		}
	}

	coordinates
}

#[cfg(test)]
mod tests {

	use super::*;
	use std::env;
	use std::error::Error;
	use std::fs::File;
	use std::io::prelude::*;
	use std::path;

	#[test]
	fn test_load() {
		let glider: Vec<Coordinate> = vec![
			(0, 1).into(),
			(1, 0).into(),
			(1, 2).into(),
			(2, 0).into(),
			(2, 1).into(),
		];

		assert_eq!(get_life("glider.life"), glider);
	}

	fn get_life(name: &str) -> Vec<Coordinate> {
		load_life(get_file(name))
	}

	fn get_file(name: &str) -> String {
		let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

		let mut path = path::PathBuf::from(manifest_dir);
		path.push("resources");
		path.push(name);
		let display = path.display();

		let mut file = match File::open(&path) {
			// The `description` method of `io::Error` returns a string that
			// describes the error
			Err(why) => panic!("couldn't open {}: {}", display, why.description()),
			Ok(file) => file,
		};

		let mut file_content = String::new();

		match file.read_to_string(&mut file_content) {
			Err(why) => panic!("couldn't read {}: {}", display, why.description()),
			Ok(_) => file_content,
		}
	}

}
