use ggez::conf;
use ggez::event::{self, Keycode, Mod};
use ggez::graphics;
use ggez::graphics::Rect;
use ggez::timer;
use ggez::{Context, GameResult};
use grid::Grid;
use input::{InputAction, InputState};
use loader;
use std::env;
use std::io::Read;
use std::path::{self, Path};
use view::View;

pub struct GameState {
    pub grid: Grid,
    pub view: View,
    running: bool,
    input_state: InputState,
}

impl GameState {
    pub fn new() -> GameState {
        let mut input_state = InputState::new();

        input_state.set_binding(InputAction::Up, Keycode::W, true);
        input_state.set_binding(InputAction::Down, Keycode::S, true);
        input_state.set_binding(InputAction::Left, Keycode::A, true);
        input_state.set_binding(InputAction::Right, Keycode::D, true);

        input_state.set_binding(InputAction::NudgeUp, Keycode::Up, false);
        input_state.set_binding(InputAction::NudgeDown, Keycode::Down, false);
        input_state.set_binding(InputAction::NudgeLeft, Keycode::Left, false);
        input_state.set_binding(InputAction::NudgeRight, Keycode::Right, false);

        input_state.set_binding(InputAction::ZoomIn, Keycode::Q, true);
        input_state.set_binding(InputAction::ZoomOut, Keycode::E, true);

        input_state.set_binding(InputAction::FlipCell, Keycode::Space, false);

        input_state.set_binding(InputAction::Quit, Keycode::Escape, false);
        input_state.set_binding(InputAction::Pause, Keycode::P, false);

        GameState {
            grid: Grid::new(),
            running: false,
            view: View::new(),
            input_state,
        }
    }

    pub fn flip_cursor_state(&mut self) {
        self.grid.flip_state(&self.view.cursor_pos);
    }
}

impl event::EventHandler for GameState {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        const DESIRED_FPS: u32 = 10;

        if self.input_state.is_active(InputAction::Quit) {
            ctx.quit()?;
        }
        if self.input_state.is_active(InputAction::Pause) {
            self.running = !self.running;
        }

        if self.input_state.is_active(InputAction::FlipCell) {
            self.flip_cursor_state();
        }

        if self.input_state.is_active(InputAction::Up)
            || self.input_state.is_active(InputAction::NudgeUp)
        {
            self.view.pan(-1, 0);
        }
        if self.input_state.is_active(InputAction::Down)
            || self.input_state.is_active(InputAction::NudgeDown)
        {
            self.view.pan(1, 0);
        }
        if self.input_state.is_active(InputAction::Left)
            || self.input_state.is_active(InputAction::NudgeLeft)
        {
            self.view.pan(0, -1);
        }
        if self.input_state.is_active(InputAction::Right)
            || self.input_state.is_active(InputAction::NudgeRight)
        {
            self.view.pan(0, 1);
        }

        if self.input_state.is_active(InputAction::ZoomIn) {
            self.view.zoom_in();
        }
        if self.input_state.is_active(InputAction::ZoomOut) {
            self.view.zoom_out();
        }

        while timer::check_update_time(ctx, DESIRED_FPS) {
            if self.running {
                self.grid.advance();
            }
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::set_background_color(ctx, graphics::BLACK);
        graphics::clear(ctx);

        self.view.draw(&mut self.grid, ctx)?;

        graphics::present(ctx);

        Ok(())
    }

    fn key_up_event(
        &mut self,
        _ctx: &mut Context,
        key_code: Keycode,
        _key_mod: Mod,
        _repeat: bool,
    ) {
        self.input_state.on_key_change(key_code, false);
    }

    fn key_down_event(
        &mut self,
        _ctx: &mut Context,
        key_code: Keycode,
        _keymod: Mod,
        _repeat: bool,
    ) {
        self.input_state.on_key_change(key_code, true);
    }

    fn resize_event(&mut self, ctx: &mut Context, width: u32, height: u32) {
        self.view.resize(width, height);
        graphics::set_screen_coordinates(ctx, Rect::new(0.0, 0.0, width as f32, height as f32))
            .unwrap();
    }
}

pub fn run() {
    let mut c = conf::Conf::new();

    c.window_setup.resizable = true;
    c.window_setup.allow_highdpi = false;

    let ctx = &mut Context::load_from_conf("Game of life", "dan", c).unwrap();

    // We add the CARGO_MANIFEST_DIR/resources do the filesystems paths so
    // we we look in the cargo project for files.
    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        ctx.filesystem.mount(&path, true);
    }

    println!("{}", graphics::get_renderer_info(ctx).unwrap());

    let state = &mut GameState::new();

    let mut file = ctx.filesystem.open(Path::new("/rle1.life")).unwrap();
    let mut rle1 = String::new();
    file.read_to_string(&mut rle1).unwrap();

    state.grid.add_cells(loader::load_life(rle1));

    if let Err(e) = event::run(ctx, state) {
        println!("Error encountered: {}", e);
    } else {
        println!("Game exited cleanly.");
    }
}
