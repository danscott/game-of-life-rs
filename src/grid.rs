use std::collections::HashMap;
use std::fmt::{Display, Error, Formatter};

struct Cell {
    pub live_neighbours: u8,
    pub is_live: bool,
}

impl Cell {
    pub fn alive() -> Cell {
        Cell {
            live_neighbours: 0,
            is_live: true,
        }
    }

    pub fn dead() -> Cell {
        Cell {
            live_neighbours: 0,
            is_live: false,
        }
    }

    pub fn dead_neighbour() -> Cell {
        Cell {
            live_neighbours: 1,
            is_live: false,
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub struct Coordinate {
    pub row: isize,
    pub col: isize,
}

impl From<(isize, isize)> for Coordinate {
    fn from((row, col): (isize, isize)) -> Self {
        Coordinate { row, col }
    }
}

impl Display for Coordinate {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "(r:{} c:{})", self.row, self.col)
    }
}

pub struct Region {
    pub top_left: Coordinate,
    pub bottom_right: Coordinate,
}

impl Region {
    pub fn contains(&self, coordinate: &Coordinate) -> bool {
        coordinate.row >= self.top_left.row
            && coordinate.row <= self.bottom_right.row
            && coordinate.col >= self.top_left.col
            && coordinate.col <= self.bottom_right.col
    }
}

impl From<(isize, isize, isize, isize)> for Region {
    fn from((min_row, min_col, max_row, max_col): (isize, isize, isize, isize)) -> Self {
        Region {
            top_left: (min_row, min_col).into(),
            bottom_right: (max_row, max_col).into(),
        }
    }
}

impl Display for Region {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "[tl:{} br:{}]", self.top_left, self.bottom_right)
    }
}

pub struct Grid {
    cells: HashMap<Coordinate, Cell>,
}

impl Grid {
    pub fn new() -> Grid {
        Grid {
            cells: HashMap::new(),
        }
    }

    pub fn add_cells(&mut self, cells: Vec<Coordinate>) {
        for coordinate in cells {
            self.make_live(&coordinate);
        }
    }

    pub fn advance(&mut self) {
        let mut new_grid = Grid::new();

        self.cells
            .iter()
            .filter(|(_, c)| {
                (c.is_live && (c.live_neighbours == 2 || c.live_neighbours == 3))
                    || (!c.is_live && c.live_neighbours == 3)
            }).for_each(|(c, _)| new_grid.make_live(c));

        self.cells = new_grid.cells;
    }

    pub fn live_cells_within(&mut self, region: &Region) -> Vec<Coordinate> {
        self.cells
            .iter()
            .filter_map(|(coordinate, cell)| {
                if cell.is_live && region.contains(coordinate) {
                    return Some(coordinate.clone());
                }

                None
            }).collect()
    }

    pub fn is_cell_live(&self, coordinate: &Coordinate) -> bool {
        match self.cells.get(coordinate) {
            Some(cell) => cell.is_live,
            _ => false,
        }
    }

    pub fn flip_state(&mut self, coordinate: &Coordinate) {
        if self.is_cell_live(coordinate) {
            self.make_dead(coordinate);
        } else {
            self.make_live(coordinate);
        }
    }

    pub fn make_live(&mut self, coordinate: &Coordinate) {
        self.cells
            .entry(*coordinate)
            .and_modify(|c| c.is_live = true)
            .or_insert(Cell::alive());

        self.inc_neighbours(coordinate);
    }

    pub fn make_dead(&mut self, coordinate: &Coordinate) {
        self.cells
            .entry(*coordinate)
            .and_modify(|c| c.is_live = false)
            .or_insert(Cell::dead());

        self.dec_neighbours(coordinate);
    }

    fn inc_neighbours(&mut self, coordinate: &Coordinate) {
        for &neighbour in neighbour_coordinates(coordinate).iter() {
            self.cells
                .entry(neighbour)
                .and_modify(|c| c.live_neighbours += 1)
                .or_insert(Cell::dead_neighbour());
        }
    }

    fn dec_neighbours(&mut self, coordinate: &Coordinate) {
        for neighbour in neighbour_coordinates(coordinate).iter() {
            match self.cells.get_mut(neighbour) {
                Some(cell) => {
                    if cell.live_neighbours > 0 {
                        cell.live_neighbours -= 1;
                    }
                }
                _ => (),
            }
        }
    }
}

fn neighbour_coordinates(coordinate: &Coordinate) -> [Coordinate; 8] {
    let Coordinate { row, col } = *coordinate;
    [
        (row - 1, col - 1).into(),
        (row - 1, col).into(),
        (row - 1, col + 1).into(),
        (row, col - 1).into(),
        (row, col + 1).into(),
        (row + 1, col - 1).into(),
        (row + 1, col).into(),
        (row + 1, col + 1).into(),
    ]
}
