extern crate console;
extern crate ggez;

mod game;
mod grid;
mod input;
mod loader;
mod view;

fn main() {
    game::run();
}
